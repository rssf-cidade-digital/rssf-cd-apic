﻿using API_Células.ModelosExibição;
using Modelos.Modelos;
using Microsoft.IdentityModel.Tokens;
using System;
using System.Collections.Generic;
using System.IdentityModel.Tokens.Jwt;
using System.Linq;
using System.Security.Claims;
using System.Text;
using System.Threading.Tasks;
using Modelos.ModelosExibição;

namespace API_Células.Serviços
{
    public static class TokenServiço
    {
        public static string GerarToken(UsuárioModeloExibição usuário)
        {
            var gerenteToken = new JwtSecurityTokenHandler();
            var chave = Encoding.ASCII.GetBytes(Configurações.ChaveJWT);

            var descriçãoToken = new SecurityTokenDescriptor()
            {
                Subject = new ClaimsIdentity(new Claim[]
                {
                    new Claim(ClaimTypes.Name, usuário.Nome),
                    new Claim(ClaimTypes.Role, usuário.Nível.Valor.ToString())
                }),
                Expires = DateTime.UtcNow.AddHours(2),
                SigningCredentials = new SigningCredentials(new SymmetricSecurityKey(chave), SecurityAlgorithms.HmacSha256Signature)
            };

            var token = gerenteToken.CreateToken(descriçãoToken);
            return gerenteToken.WriteToken(token);
        }
    }
}
