﻿using Modelos.Interfaces;
using Modelos.Modelos;
using API_Células.ModelosExibição;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Modelos.Bases;

namespace API_Células.Controllers
{
    [Route("requisicoes/atendidas")]
    public class RequisiçõesAtendidasController : Controller
    {
        private readonly BaseRequisiçõesRepositório _repositórioRequisições;
        private readonly BaseRequisiçõesAtendidasRepositório _repositório;

        public RequisiçõesAtendidasController(BaseRequisiçõesRepositório repositórioRequisições, 
            BaseRequisiçõesAtendidasRepositório repositório)
        {
            _repositórioRequisições = repositórioRequisições;
            _repositório = repositório;
        }

        [HttpGet]
        [Authorize(Roles = "2")]
        [Route("ver/ultima/{idCelula}")]
        public async Task<ActionResult<RequisiçãoAtendidaModeloExibição>> VerÚltimaRequisição(int idCelula)
        {
            var últimaRequisição = _repositórioRequisições.RetornarÚltimo(ordenarPeloId: true);
            var requisiçãoAtendida = _repositório.RetornarÚnico(r => r.Célula.Id == idCelula && r.Requisição.Id == últimaRequisição.Id);

            if(requisiçãoAtendida == null)
            {
                return new RequisiçãoAtendidaModeloExibição() { Requisição = últimaRequisição, CélulaAtendeu = false };
            }

            return new RequisiçãoAtendidaModeloExibição() { Requisição = últimaRequisição, CélulaAtendeu = true };
        }

        [HttpGet]
        [Authorize(Roles = "2")]
        [Route("ver/celula/{idCelula}")]
        public async Task<ActionResult<List<RequisiçãoModelo>>> ListarRequisiçõesAtendidas(int idCelula)
        {
            var atendidas = _repositório.RetornarLista(r => r.Célula.Id == idCelula);
            var requisiçõesAtendidas = new List<RequisiçãoModelo>();

            foreach (var item in atendidas)
            {
                requisiçõesAtendidas.Add(item.Requisição);
            }

            return requisiçõesAtendidas;
        }

    }
}
