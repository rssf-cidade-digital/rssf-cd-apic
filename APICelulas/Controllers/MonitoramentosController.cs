﻿using Modelos.Interfaces;
using API_Células.ModelosExibição;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Modelos.Modelos;
using Modelos.Bases;
using Modelos.Bibliotecas;

namespace API_Células.Controllers
{
    [Route("monitoramentos")]
    public class MonitoramentosController : Controller
    {
        private readonly string _mensagemTemperatura = "Alerta de temperatura em sua região: {0:0.00} °C. É recomendável que beba líquidos.";
        private readonly string _mensagemUmidade = "Alerta de umidade em sua região: {0:0.00}%. É recomendável que use umidificadores.";
        private readonly string _mensagemAfastamento = "Alerta de {0} em sua região. É recomendável que se afaste de local.";
        private readonly string _mensagemRadiaçãoUV = "Alerta de alta radiação UV em sua região. É recomendável que se proteja do sol com " +
            "vestimentas adequadas, óculos escuros e protetor solar.";

        private readonly string _mensagemRequisiçãoJáAtendida = "Essa requisição já foi atendida";
        private readonly BaseRequisiçõesRepositório _repositórioRequisições;
        private readonly BaseRequisiçõesAtendidasRepositório _repositórioRequisiçõesAtendidas;
        private readonly BaseCélulasRepositório _repositórioCélulas;
        private readonly BaseAlertasRepositório _repositórioAlertas;
        private readonly BaseNíveisAlertasRepositório _repositórioNíveisAlertas;
        private readonly BaseMonitoramentosRepositório _repositório;

        public MonitoramentosController(BaseRequisiçõesRepositório repositórioRequisições,
            BaseRequisiçõesAtendidasRepositório repositórioRequisiçõesAtendidas, BaseCélulasRepositório repositórioCélulas, 
            BaseMonitoramentosRepositório repositório, BaseAlertasRepositório repositórioAlertas, 
            BaseNíveisAlertasRepositório repositórioNíveisAlertas)
        {
            _repositórioRequisições = repositórioRequisições;
            _repositórioRequisiçõesAtendidas = repositórioRequisiçõesAtendidas;
            _repositórioCélulas = repositórioCélulas;
            _repositórioAlertas = repositórioAlertas;
            _repositórioNíveisAlertas = repositórioNíveisAlertas;
            _repositório = repositório;
        }

        [HttpPost]
        [Authorize(Roles = "2")]
        [Route("criar/{idCelula}/{idRequisicao}")]
        public async Task<ActionResult<MensagemModeloExibição>> CriarMonitoramento(int idCelula, int idRequisicao,
            [FromBody] MonitoramentoModelo monitoramento)
        {
            var requisiçãoJáAtendida = _repositórioRequisiçõesAtendidas
                .RetornarÚnico(r => r.Célula.Id == idCelula && r.Requisição.Id == idRequisicao) != null;

            if (requisiçãoJáAtendida)
            {
                return new MensagemModeloExibição(_mensagemRequisiçãoJáAtendida);
            }

            var célula = _repositórioCélulas.RetornarPeloId(idCelula);
            var requisição = _repositórioRequisições.RetornarPeloId(idRequisicao);

            if (!GravarMonitoramento(célula, requisição, monitoramento))
            {
                return new MensagemModeloExibição(MensagemModeloExibição.mensagemFracasso);
            }

            if (!GravarRequisiçãoAtendida(célula, requisição))
            {
                return new MensagemModeloExibição(MensagemModeloExibição.mensagemFracasso);
            }

            return ChecarAvisos(monitoramento);
        }

        private MensagemModeloExibição ChecarAvisos(MonitoramentoModelo monitoramento)
        {
            var sucessoGravações = true;
            if(CritériosAlertaBiblioteca.ChecarTemperatura(monitoramento.Temperatura))
            {
                var mensagem = String.Format(_mensagemTemperatura, monitoramento.Temperatura);
                sucessoGravações = sucessoGravações && GravarAlerta(mensagem, monitoramento);
            }

            if(CritériosAlertaBiblioteca.ChecarUmidade(monitoramento.Umidade))
            {
                var mensagem = String.Format(_mensagemUmidade, monitoramento.Umidade);
                sucessoGravações = sucessoGravações && GravarAlerta(mensagem, monitoramento);
            }

            if (CritériosAlertaBiblioteca.ChecarNívelGás(monitoramento.GásTóxico))
            {
                var mensagem = String.Format(_mensagemAfastamento, "gás tóxico");
                sucessoGravações = sucessoGravações && GravarAlerta(mensagem, monitoramento);
            }

            if(CritériosAlertaBiblioteca.ChecarNívelGás(monitoramento.Fumaça))
            {
                var mensagem = String.Format(_mensagemAfastamento, "gás inflamável");
                sucessoGravações = sucessoGravações && GravarAlerta(mensagem, monitoramento);
            }

            if(CritériosAlertaBiblioteca.ChecarIncêndio(monitoramento.Incêndio))
            {
                var mensagem = String.Format(_mensagemAfastamento, "incêndio");
                sucessoGravações = sucessoGravações && GravarAlerta(mensagem, monitoramento);
            }

            if(CritériosAlertaBiblioteca.ChecarRadiaçãoUV(monitoramento.RadiaçãoUV))
            {
                sucessoGravações = sucessoGravações && GravarAlerta(_mensagemRadiaçãoUV, monitoramento);
            }

            if(sucessoGravações)
            {
                return new MensagemModeloExibição(MensagemModeloExibição.mensagemSucesso);
            }

            return new MensagemModeloExibição(MensagemModeloExibição.mensagemFracasso);
        }

        private bool GravarAlerta(string mensagem, MonitoramentoModelo monitoramento)
        {

            //Mensagem no Telegram vem aqui

            var nível = _repositórioNíveisAlertas.RetornarÚnico(n => n.Valor == 1);
            var alerta = new AlertaModelo(monitoramento, nível, mensagem);

            return _repositórioAlertas.InserirUm(alerta);
        }

        private bool GravarMonitoramento(CélulaModelo célula, RequisiçãoModelo requisição, MonitoramentoModelo monitoramento)
        {
            monitoramento.Célula = célula;
            monitoramento.Requisição = requisição;
            monitoramento.DataHora = DateTime.Now;

            return _repositório.InserirUm(monitoramento);
        }

        private bool GravarRequisiçãoAtendida(CélulaModelo célula, RequisiçãoModelo requisição)
        {
            var requisiçãoAtendida = new RequisiçãoAtendidaModelo() { Célula = célula, Requisição = requisição };
            return _repositórioRequisiçõesAtendidas.InserirUm(requisiçãoAtendida);
        }
    }
}
