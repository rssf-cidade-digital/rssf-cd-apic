﻿using Modelos.Interfaces;
using Modelos.Modelos;
using API_Células.Serviços;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Modelos.ModelosExibição;
using API_Células.ModelosExibição;
using Modelos.Bases;

namespace API_Células.Controllers
{
    [Route("contas")]
    public class ContasController : Controller
    {
        private BaseUsuáriosRepositório _repositório;

        public ContasController(BaseUsuáriosRepositório repositório)
        {
            _repositório = repositório;
        }

        [HttpPost]
        [AllowAnonymous]
        [Route("login")]
        public async Task<ActionResult<TokenModeloExibição>> Autenticar([FromBody] LoginModeloExibição login)
        {
            var requisiçãoLogin = _repositório.Logar(login);

            if(requisiçãoLogin == null)
            {
                return NotFound();
            }

            var token = TokenServiço.GerarToken(requisiçãoLogin);
            return new TokenModeloExibição(requisiçãoLogin, token);
        }
    }
}
