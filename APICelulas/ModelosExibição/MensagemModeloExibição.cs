﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace API_Células.ModelosExibição
{
    public class MensagemModeloExibição
    {
        public static readonly string mensagemSucesso = "Operação realizada com sucesso.";
        public static readonly string mensagemFracasso = "Erro na operação.";
        public static readonly string mensagemNãoVálido = "Há campos inválidos";

        public string Mensagem { get; set; }
        public MensagemModeloExibição(string mensagem)
        {
            Mensagem = mensagem;
        }

        
    }
}
