﻿using Modelos.ModelosExibição;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace API_Células.ModelosExibição
{
    public class TokenModeloExibição
    {
        public UsuárioModeloExibição Usuário { get; set; }
        public string Token { get; set; }

        public TokenModeloExibição(UsuárioModeloExibição usuário, string token)
        {
            Usuário = usuário;
            Token = token;
        }


    }
}
