﻿using Modelos.Modelos;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace API_Células.ModelosExibição
{
    public class RequisiçãoAtendidaModeloExibição
    {
        public RequisiçãoModelo Requisição { get; set; }
        public bool CélulaAtendeu { get; set; }
    }
}
