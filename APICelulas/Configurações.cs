﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace API_Células
{
    public static class Configurações
    {
#if DEBUG || RELEASE || EF
        private static readonly EnvironmentVariableTarget alvo = EnvironmentVariableTarget.User;
#elif DOCKER
        private static readonly EnvironmentVariableTarget alvo = EnvironmentVariableTarget.Process;
#endif
        public static string ChaveJWT { get => Environment.GetEnvironmentVariable("ChaveJWT", alvo); }
    }
}
