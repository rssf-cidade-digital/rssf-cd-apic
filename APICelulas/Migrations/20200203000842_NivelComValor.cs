﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace API_Células.Migrations
{
    public partial class NivelComValor : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<int>(
                name: "Valor",
                table: "NíveisUsuários",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.AddColumn<int>(
                name: "Valor",
                table: "NíveisAlerta",
                nullable: false,
                defaultValue: 0);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "Valor",
                table: "NíveisUsuários");

            migrationBuilder.DropColumn(
                name: "Valor",
                table: "NíveisAlerta");
        }
    }
}
