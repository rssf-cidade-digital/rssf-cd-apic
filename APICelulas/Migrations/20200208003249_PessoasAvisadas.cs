﻿using Microsoft.EntityFrameworkCore.Metadata;
using Microsoft.EntityFrameworkCore.Migrations;

namespace API_Células.Migrations
{
    public partial class PessoasAvisadas : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "Descrição",
                table: "PessoasAvisar");

            migrationBuilder.AddColumn<string>(
                name: "IdChatTelegram",
                table: "PessoasAvisar",
                nullable: false);

            migrationBuilder.CreateTable(
                name: "PessoasAvisadas",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("MySql:ValueGenerationStrategy", MySqlValueGenerationStrategy.IdentityColumn),
                    AlertaId = table.Column<int>(nullable: false),
                    PessoaId = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_PessoasAvisadas", x => x.Id);
                    table.ForeignKey(
                        name: "FK_PessoasAvisadas_Alertas_AlertaId",
                        column: x => x.AlertaId,
                        principalTable: "Alertas",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_PessoasAvisadas_PessoasAvisar_PessoaId",
                        column: x => x.PessoaId,
                        principalTable: "PessoasAvisar",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateIndex(
                name: "IX_PessoasAvisadas_AlertaId",
                table: "PessoasAvisadas",
                column: "AlertaId");

            migrationBuilder.CreateIndex(
                name: "IX_PessoasAvisadas_PessoaId",
                table: "PessoasAvisadas",
                column: "PessoaId");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "PessoasAvisadas");

            migrationBuilder.DropColumn(
                name: "IdChatTelegram",
                table: "PessoasAvisar");

            migrationBuilder.AddColumn<string>(
                name: "Descrição",
                table: "PessoasAvisar",
                type: "longtext CHARACTER SET utf8mb4",
                nullable: false,
                defaultValue: "");
        }
    }
}
