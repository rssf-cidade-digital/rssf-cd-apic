﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace API_Células.Migrations
{
    public partial class CorrecoesPessoasAvisar : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AlterColumn<long>(
                name: "IdChatTelegram",
                table: "PessoasAvisar",
                nullable: false,
                oldClrType: typeof(string),
                oldType: "longtext CHARACTER SET utf8mb4");

            migrationBuilder.CreateIndex(
                name: "IX_PessoasAvisar_IdChatTelegram",
                table: "PessoasAvisar",
                column: "IdChatTelegram",
                unique: true);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropIndex(
                name: "IX_PessoasAvisar_IdChatTelegram",
                table: "PessoasAvisar");

            migrationBuilder.AlterColumn<string>(
                name: "IdChatTelegram",
                table: "PessoasAvisar",
                type: "longtext CHARACTER SET utf8mb4",
                nullable: false,
                oldClrType: typeof(long));
        }
    }
}
