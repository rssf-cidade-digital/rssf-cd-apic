﻿using Microsoft.EntityFrameworkCore.Metadata;
using Microsoft.EntityFrameworkCore.Migrations;

namespace API_Células.Migrations
{
    public partial class RequisicoesAtendidas : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "Atendida",
                table: "Requisições");

            migrationBuilder.CreateTable(
                name: "RequisiçõesAtendidas",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("MySql:ValueGenerationStrategy", MySqlValueGenerationStrategy.IdentityColumn),
                    CélulaId = table.Column<int>(nullable: true),
                    RequisiçãoId = table.Column<int>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_RequisiçõesAtendidas", x => x.Id);
                    table.ForeignKey(
                        name: "FK_RequisiçõesAtendidas_Células_CélulaId",
                        column: x => x.CélulaId,
                        principalTable: "Células",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_RequisiçõesAtendidas_Requisições_RequisiçãoId",
                        column: x => x.RequisiçãoId,
                        principalTable: "Requisições",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateIndex(
                name: "IX_RequisiçõesAtendidas_CélulaId",
                table: "RequisiçõesAtendidas",
                column: "CélulaId");

            migrationBuilder.CreateIndex(
                name: "IX_RequisiçõesAtendidas_RequisiçãoId",
                table: "RequisiçõesAtendidas",
                column: "RequisiçãoId");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "RequisiçõesAtendidas");

            migrationBuilder.AddColumn<bool>(
                name: "Atendida",
                table: "Requisições",
                type: "tinyint(1)",
                nullable: false,
                defaultValue: false);
        }
    }
}
