﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace API_Células.Migrations
{
    public partial class Célula : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AlterColumn<string>(
                name: "Endereço",
                table: "Células",
                nullable: true,
                oldClrType: typeof(double),
                oldType: "double");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AlterColumn<double>(
                name: "Endereço",
                table: "Células",
                type: "double",
                nullable: false,
                oldClrType: typeof(string),
                oldNullable: true);
        }
    }
}
