﻿using System;
using Microsoft.EntityFrameworkCore.Metadata;
using Microsoft.EntityFrameworkCore.Migrations;

namespace API_Células.Migrations
{
    public partial class Início : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "Células",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("MySql:ValueGenerationStrategy", MySqlValueGenerationStrategy.IdentityColumn),
                    EndereçoMac = table.Column<string>(nullable: true),
                    Latitude = table.Column<double>(nullable: false),
                    Longitude = table.Column<double>(nullable: false),
                    Endereço = table.Column<double>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Células", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "Requisições",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("MySql:ValueGenerationStrategy", MySqlValueGenerationStrategy.IdentityColumn),
                    Momento = table.Column<DateTime>(nullable: false),
                    Atendida = table.Column<bool>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Requisições", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "Monitoramentos",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("MySql:ValueGenerationStrategy", MySqlValueGenerationStrategy.IdentityColumn),
                    CélulaId = table.Column<int>(nullable: true),
                    RequisiçãoId = table.Column<int>(nullable: true),
                    Temperatura = table.Column<double>(nullable: false),
                    Umidade = table.Column<double>(nullable: false),
                    GásTóxico = table.Column<double>(nullable: false),
                    Fumaça = table.Column<double>(nullable: false),
                    Incêndio = table.Column<double>(nullable: false),
                    RadiaçãoUV = table.Column<double>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Monitoramentos", x => x.Id);
                    table.ForeignKey(
                        name: "FK_Monitoramentos_Células_CélulaId",
                        column: x => x.CélulaId,
                        principalTable: "Células",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_Monitoramentos_Requisições_RequisiçãoId",
                        column: x => x.RequisiçãoId,
                        principalTable: "Requisições",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "Alertas",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("MySql:ValueGenerationStrategy", MySqlValueGenerationStrategy.IdentityColumn),
                    MonitoramentoId = table.Column<int>(nullable: true),
                    MensagemAlerta = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Alertas", x => x.Id);
                    table.ForeignKey(
                        name: "FK_Alertas_Monitoramentos_MonitoramentoId",
                        column: x => x.MonitoramentoId,
                        principalTable: "Monitoramentos",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateIndex(
                name: "IX_Alertas_MonitoramentoId",
                table: "Alertas",
                column: "MonitoramentoId");

            migrationBuilder.CreateIndex(
                name: "IX_Monitoramentos_CélulaId",
                table: "Monitoramentos",
                column: "CélulaId");

            migrationBuilder.CreateIndex(
                name: "IX_Monitoramentos_RequisiçãoId",
                table: "Monitoramentos",
                column: "RequisiçãoId");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "Alertas");

            migrationBuilder.DropTable(
                name: "Monitoramentos");

            migrationBuilder.DropTable(
                name: "Células");

            migrationBuilder.DropTable(
                name: "Requisições");
        }
    }
}
