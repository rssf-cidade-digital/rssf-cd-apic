﻿using Microsoft.EntityFrameworkCore.Metadata;
using Microsoft.EntityFrameworkCore.Migrations;

namespace API_Células.Migrations
{
    public partial class NiveisAlertas : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "Nível",
                table: "Usuários");

            migrationBuilder.AddColumn<int>(
                name: "NívelId",
                table: "Usuários",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.AddColumn<int>(
                name: "NívelId",
                table: "Alertas",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.CreateTable(
                name: "NíveisAlerta",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("MySql:ValueGenerationStrategy", MySqlValueGenerationStrategy.IdentityColumn),
                    Descrição = table.Column<string>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_NíveisAlerta", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "NíveisUsuários",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("MySql:ValueGenerationStrategy", MySqlValueGenerationStrategy.IdentityColumn),
                    Descrição = table.Column<string>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_NíveisUsuários", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "PessoasAvisar",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("MySql:ValueGenerationStrategy", MySqlValueGenerationStrategy.IdentityColumn),
                    CélulaId = table.Column<int>(nullable: false),
                    Descrição = table.Column<string>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_PessoasAvisar", x => x.Id);
                    table.ForeignKey(
                        name: "FK_PessoasAvisar_Células_CélulaId",
                        column: x => x.CélulaId,
                        principalTable: "Células",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateIndex(
                name: "IX_Usuários_NívelId",
                table: "Usuários",
                column: "NívelId");

            migrationBuilder.CreateIndex(
                name: "IX_Alertas_NívelId",
                table: "Alertas",
                column: "NívelId");

            migrationBuilder.CreateIndex(
                name: "IX_PessoasAvisar_CélulaId",
                table: "PessoasAvisar",
                column: "CélulaId");

            migrationBuilder.AddForeignKey(
                name: "FK_Alertas_NíveisAlerta_NívelId",
                table: "Alertas",
                column: "NívelId",
                principalTable: "NíveisAlerta",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_Usuários_NíveisUsuários_NívelId",
                table: "Usuários",
                column: "NívelId",
                principalTable: "NíveisUsuários",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Alertas_NíveisAlerta_NívelId",
                table: "Alertas");

            migrationBuilder.DropForeignKey(
                name: "FK_Usuários_NíveisUsuários_NívelId",
                table: "Usuários");

            migrationBuilder.DropTable(
                name: "NíveisAlerta");

            migrationBuilder.DropTable(
                name: "NíveisUsuários");

            migrationBuilder.DropTable(
                name: "PessoasAvisar");

            migrationBuilder.DropIndex(
                name: "IX_Usuários_NívelId",
                table: "Usuários");

            migrationBuilder.DropIndex(
                name: "IX_Alertas_NívelId",
                table: "Alertas");

            migrationBuilder.DropColumn(
                name: "NívelId",
                table: "Usuários");

            migrationBuilder.DropColumn(
                name: "NívelId",
                table: "Alertas");

            migrationBuilder.AddColumn<string>(
                name: "Nível",
                table: "Usuários",
                type: "longtext CHARACTER SET utf8mb4",
                nullable: false,
                defaultValue: "");
        }
    }
}
