﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace API_Células.Migrations
{
    public partial class CamposObrigatórios : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Alertas_Monitoramentos_MonitoramentoId",
                table: "Alertas");

            migrationBuilder.DropForeignKey(
                name: "FK_Monitoramentos_Células_CélulaId",
                table: "Monitoramentos");

            migrationBuilder.DropForeignKey(
                name: "FK_Monitoramentos_Requisições_RequisiçãoId",
                table: "Monitoramentos");

            migrationBuilder.AlterColumn<string>(
                name: "Usuário",
                table: "Usuários",
                nullable: false,
                oldClrType: typeof(string),
                oldType: "longtext CHARACTER SET utf8mb4",
                oldNullable: true);

            migrationBuilder.AlterColumn<string>(
                name: "Senha",
                table: "Usuários",
                nullable: false,
                oldClrType: typeof(string),
                oldType: "longtext CHARACTER SET utf8mb4",
                oldNullable: true);

            migrationBuilder.AlterColumn<string>(
                name: "Nível",
                table: "Usuários",
                nullable: false,
                oldClrType: typeof(string),
                oldType: "longtext CHARACTER SET utf8mb4",
                oldNullable: true);

            migrationBuilder.AlterColumn<string>(
                name: "Nome",
                table: "Usuários",
                nullable: false,
                oldClrType: typeof(string),
                oldType: "longtext CHARACTER SET utf8mb4",
                oldNullable: true);

            migrationBuilder.AlterColumn<int>(
                name: "RequisiçãoId",
                table: "Monitoramentos",
                nullable: false,
                oldClrType: typeof(int),
                oldType: "int",
                oldNullable: true);

            migrationBuilder.AlterColumn<int>(
                name: "CélulaId",
                table: "Monitoramentos",
                nullable: false,
                oldClrType: typeof(int),
                oldType: "int",
                oldNullable: true);

            migrationBuilder.AlterColumn<string>(
                name: "EndereçoMac",
                table: "Células",
                nullable: false,
                oldClrType: typeof(string),
                oldType: "longtext CHARACTER SET utf8mb4",
                oldNullable: true);

            migrationBuilder.AlterColumn<string>(
                name: "Endereço",
                table: "Células",
                nullable: false,
                oldClrType: typeof(string),
                oldType: "longtext CHARACTER SET utf8mb4",
                oldNullable: true);

            migrationBuilder.AlterColumn<int>(
                name: "MonitoramentoId",
                table: "Alertas",
                nullable: false,
                oldClrType: typeof(int),
                oldType: "int",
                oldNullable: true);

            migrationBuilder.AlterColumn<string>(
                name: "MensagemAlerta",
                table: "Alertas",
                nullable: false,
                oldClrType: typeof(string),
                oldType: "longtext CHARACTER SET utf8mb4",
                oldNullable: true);

            migrationBuilder.AddForeignKey(
                name: "FK_Alertas_Monitoramentos_MonitoramentoId",
                table: "Alertas",
                column: "MonitoramentoId",
                principalTable: "Monitoramentos",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_Monitoramentos_Células_CélulaId",
                table: "Monitoramentos",
                column: "CélulaId",
                principalTable: "Células",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_Monitoramentos_Requisições_RequisiçãoId",
                table: "Monitoramentos",
                column: "RequisiçãoId",
                principalTable: "Requisições",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Alertas_Monitoramentos_MonitoramentoId",
                table: "Alertas");

            migrationBuilder.DropForeignKey(
                name: "FK_Monitoramentos_Células_CélulaId",
                table: "Monitoramentos");

            migrationBuilder.DropForeignKey(
                name: "FK_Monitoramentos_Requisições_RequisiçãoId",
                table: "Monitoramentos");

            migrationBuilder.AlterColumn<string>(
                name: "Usuário",
                table: "Usuários",
                type: "longtext CHARACTER SET utf8mb4",
                nullable: true,
                oldClrType: typeof(string));

            migrationBuilder.AlterColumn<string>(
                name: "Senha",
                table: "Usuários",
                type: "longtext CHARACTER SET utf8mb4",
                nullable: true,
                oldClrType: typeof(string));

            migrationBuilder.AlterColumn<string>(
                name: "Nível",
                table: "Usuários",
                type: "longtext CHARACTER SET utf8mb4",
                nullable: true,
                oldClrType: typeof(string));

            migrationBuilder.AlterColumn<string>(
                name: "Nome",
                table: "Usuários",
                type: "longtext CHARACTER SET utf8mb4",
                nullable: true,
                oldClrType: typeof(string));

            migrationBuilder.AlterColumn<int>(
                name: "RequisiçãoId",
                table: "Monitoramentos",
                type: "int",
                nullable: true,
                oldClrType: typeof(int));

            migrationBuilder.AlterColumn<int>(
                name: "CélulaId",
                table: "Monitoramentos",
                type: "int",
                nullable: true,
                oldClrType: typeof(int));

            migrationBuilder.AlterColumn<string>(
                name: "EndereçoMac",
                table: "Células",
                type: "longtext CHARACTER SET utf8mb4",
                nullable: true,
                oldClrType: typeof(string));

            migrationBuilder.AlterColumn<string>(
                name: "Endereço",
                table: "Células",
                type: "longtext CHARACTER SET utf8mb4",
                nullable: true,
                oldClrType: typeof(string));

            migrationBuilder.AlterColumn<int>(
                name: "MonitoramentoId",
                table: "Alertas",
                type: "int",
                nullable: true,
                oldClrType: typeof(int));

            migrationBuilder.AlterColumn<string>(
                name: "MensagemAlerta",
                table: "Alertas",
                type: "longtext CHARACTER SET utf8mb4",
                nullable: true,
                oldClrType: typeof(string));

            migrationBuilder.AddForeignKey(
                name: "FK_Alertas_Monitoramentos_MonitoramentoId",
                table: "Alertas",
                column: "MonitoramentoId",
                principalTable: "Monitoramentos",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_Monitoramentos_Células_CélulaId",
                table: "Monitoramentos",
                column: "CélulaId",
                principalTable: "Células",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_Monitoramentos_Requisições_RequisiçãoId",
                table: "Monitoramentos",
                column: "RequisiçãoId",
                principalTable: "Requisições",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }
    }
}
